import React, { Component } from "react";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import Home from "./Components/Home/Home";
import Footer from "./Components/Footer/Footer";
import Navbar from "./Components/Navigation/Navbar";

import RestuarantPage from "./Components/RestuarantPage/RestuarantPage";

export class App extends Component {
  render() {
    return (
      <div>
        <Navbar />

        <BrowserRouter>
          <Switch>
            <Route exact path="/" component={Home} />
            <Route
              path="/restaurant/:restaurantID/order"
              component={RestuarantPage}
            />
          </Switch>
        </BrowserRouter>

        <Footer />
      </div>
    );
  }
}

export default App;
