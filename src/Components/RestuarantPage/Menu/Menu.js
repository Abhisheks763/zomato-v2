import React, { Component } from "react";
import MenuItem from "../MenuItem/MenuItem";
import { getFood } from "../../../apiCalls/apiCalls";
export class Menu extends Component {
  constructor() {
    super();

    this.state = {
      foodItems: [],
    };
  }

  incrementHandler = (e) => {
    e.target.previousElementSibling.innerText =
      parseInt(e.target.previousElementSibling.innerText) + 1;
    this.setState(
      {
        [this.props.name]: e.target.previousElementSibling.innerText,
      },
      () => {
        console.log(this.state);
      }
    );
  };

  decrementHandler = (e) => {
    if (parseInt(e.target.nextElementSibling.innerText) == 0) {
    } else {
      e.target.nextElementSibling.innerText =
        parseInt(e.target.nextElementSibling.innerText) - 1;
      this.setState({
        [this.props.name]: e.target.nextElementSibling.innerText,
      });
    }
  };

  async componentDidMount() {
    const foodData = await getFood(this.props.restaurantID);
    this.setState({ foodItems: foodData.food });
  }

  render() {
    const allFoods = this.state.foodItems.map((item) => {
      return (
        <MenuItem
          incrementHandler={this.incrementHandler}
          decrementHandler={this.decrementHandler}
          name={item.food_name}
          key={item.food_id}
          cuisine={item.cuisine}
          price={item.price}
          id={item.food_id}
        />
      );
    });

    return (
      <div className="container-lg">
        <h3 className="p-0">Order Online</h3>
        {allFoods}
      </div>
    );
  }
}

export default Menu;
