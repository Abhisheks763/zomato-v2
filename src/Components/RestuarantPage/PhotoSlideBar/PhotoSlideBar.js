import React, { Component } from "react";
import "./PhotoSlideBar.css";
import { getOneRestaurant } from "../../../apiCalls/apiCalls";

export class PhotoSlideBar extends Component {
  constructor(props) {
    super(props);

    this.state = {
      restaurantDetails: {},
    };
  }

  async componentDidMount() {
    const restaurantData = await getOneRestaurant(this.props.restaurantID);
    this.setState({ restaurantDetails: restaurantData });
    console.log(this.state);
  }

  render() {
    return (
      <div className="container-lg ">
        <div className="row">
          <div className="col-8  ">
            <img
              className="img-fluid"
              src="https://b.zmtcdn.com/data/pictures/chains/5/61555/298cd1d512282f56a9aef6aa4601ad75.jpg?fit=around|771.75:416.25&crop=771.75:416.25;*,*"
            ></img>
          </div>
          <div className="col-2 d-flex flex-column justify-content-around">
            <div className="row ">
              <img
                alt="image"
                src="https://b.zmtcdn.com/data/pictures/chains/5/61555/d50930b3a6c3089ab26b7f40389e453b.jpg?output-format=webp&amp;fit=around|300:273&amp;crop=300:273;*,*"
                loading="lazy"
                className="img-fluid"
              />
            </div>
            <div className="row ">
              <img
                alt="image"
                src="https://b.zmtcdn.com/data/pictures/2/19451542/94e86a8f4d0af4286a5de381f881fca8.jpg?fit=around|300:273&amp;crop=300:273;*,*"
                loading="lazy"
                className="img-fluid"
              />
            </div>
          </div>
          <div className="col-2 d-flex flex-column justify-content-around">
            <div className="row ">
              <img
                alt="image"
                src="https://b.zmtcdn.com/data/pictures/chains/1/60551/3dcfca13d63838a73317696f2972749d.jpg?fit=around|300:273&amp;crop=300:273;*,*"
                loading="lazy"
                className="img-fluid"
              />
            </div>
            <div className="row ">
              <img
                alt="image"
                src="https://b.zmtcdn.com/data/pictures/chains/1/60551/1d64fcac0cb49fe09615fdb9dc02c040.jpg?fit=around|300:273&amp;crop=300:273;*,*"
                loading="lazy"
                className="img-fluid"
              />
            </div>
          </div>
        </div>

        {/* Restuarant  Details */}
        <div className="container-lg sticky-top">
          <div className="row ">
            <h1 className="col-8 p-0">
              {this.state.restaurantDetails.restaurant_name}
            </h1>{" "}
            <div className="col-4 d-flex ">
              <p>Dinning rating</p>
              <p className=""> 4.0</p>
              <p>Delivery rating</p>
              <p className=""> 3.8</p>
            </div>
            <div className="row d-block m-0">
              <p className="">{this.state.restaurantDetails.address}</p>
              <p>
                Contact Number : {this.state.restaurantDetails.contact_number}
              </p>
              <p className="">Open now 10am – 3am (Today)</p>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default PhotoSlideBar;
