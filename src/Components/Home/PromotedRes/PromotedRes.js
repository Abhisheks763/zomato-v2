import { getRestaurants } from "../../../apiCalls/apiCalls";
import React, { Component } from "react";
import PromotedResCard from "../PromotedResCard/PromotedResCard";

export class PromotedRes extends Component {
  constructor(props) {
    super(props);
    this.state = {
      restaurantData: [],
    };
  }
  async componentDidMount() {
    const restaurantsData = await getRestaurants();
    this.setState({ restaurantData: restaurantsData });
  }
  render() {
    const restaurantsArray = this.state.restaurantData.map((res) => {
      return (
        <PromotedResCard
          key={res.restaurant_id}
          id={res.restaurant_id}
          name={res.restaurant_name}
          contact={res.contact_number}
          address={res.address.slice(0, 20)}
        />
      );
    });
    console.log(restaurantsArray);
    return (
      <div className="container-lg">
        <h3>Best Food in Bengaluru</h3>
        <div className="row"> {restaurantsArray}</div>
      </div>
    );
  }
}

export default PromotedRes;
