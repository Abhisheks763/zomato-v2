import "./Locations.css";
import City from "../Cities/City";
import { getCities } from "../../../apiCalls/apiCalls";

import React, { Component } from "react";

export default class Locations extends Component {
  constructor(props) {
    super(props);

    this.state = {
      citiesData: [],
    };
  }

  async componentDidMount() {
    const citiesData = await getCities();
    this.setState({ citiesData: citiesData });
  }

  render() {
    const cities = this.state.citiesData.slice(0, 100).map((city) => {
      return <City cityName={city.city_name} key={city.city_id} />;
    });

    return (
      <div style={{ margin: "50px 0px" }}>
        <h2>Cities we deliver to </h2>
        <div className="city-container">{cities}</div>
      </div>
    );
  }
}
