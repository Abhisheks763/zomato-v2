import React, { Component } from "react";
import Locality from "../Locality/Locality";
import "./PopularNear.css";

export default class PopularNear extends Component {
  render() {
    return (
      <div style={{ margin: "50px 0px" }}>
        <h4 style={{ textAlign: "center" }}>
          Popular localities in and around Delhi NCR{" "}
        </h4>
        <div className="popular-container">
          <Locality />
          <Locality />
          <Locality />
          <Locality />
          <Locality />
          <Locality />
          <Locality />
          <Locality />
          <Locality />
          <Locality />
          <Locality />
          <Locality />
          <Locality />
          <Locality />
          <Locality />
          <Locality />
          <Locality />
          <Locality />
          <Locality />
          <Locality />
          <Locality />
          <Locality />
        </div>
      </div>
    );
  }
}
