import React, { Component } from "react";
import CollectionsContainer from "./CollectionContainer/CollectionsContainer";
import Inspiration from "./Inspiration/Inspiration";
import "./Home.css";
import RestaurantsContainer from "./RestuarantContainer/RestaurantsContainer";
import Locations from "./Location/Locations";
import Explore from "./Explore/Explore";
import PopularNear from "./PopularNear/PopularNear";
import Deliveryoptions from "./DeliveryOptions/Deliveryoptions";
import Filters from "./Filters/Filters";
import PromotedRes from "./PromotedRes/PromotedRes";

export default class Home extends Component {
  render() {
    return (
      <div className="home-main">
        <Deliveryoptions />
        <Filters />
        <Inspiration />
        <PromotedRes />
        <CollectionsContainer />
        <Explore />
        <PopularNear />
        <RestaurantsContainer />
        <Locations />
      </div>
    );
  }
}
