import React, { Component } from "react";
import "./Explore.css";
import Cuisine from "../Cuisine/Cuisine";
import RestaurantType from "../RestuarantType/RestaurantType";

export default class Explore extends Component {
  render() {
    return (
      <div className="explore">
        <h3 style={{ marginBottom: "20px" }}>Explore other options</h3>
        <h4>Popular Cuisines Near Me</h4>
        <div className="popular">
          <Cuisine />
          <Cuisine />
          <Cuisine />
          <Cuisine />
          <Cuisine />
          <Cuisine />
          <Cuisine />
          <Cuisine />
          <Cuisine />
          <Cuisine />
          <Cuisine />
          <Cuisine />
          <Cuisine />
          <Cuisine />
          <Cuisine />
          <Cuisine />
          <Cuisine />
          <Cuisine />
          <Cuisine />
          <Cuisine />
          <Cuisine />
          <Cuisine />
          <Cuisine />
          <Cuisine />
        </div>

        <h4>Popular Restaurant Types Near Me</h4>
        <div className="popular">
          <RestaurantType />
          <RestaurantType />
          <RestaurantType />
          <RestaurantType />
          <RestaurantType />
          <RestaurantType />
          <RestaurantType />
          <RestaurantType />
          <RestaurantType />
          <RestaurantType />
          <RestaurantType />
          <RestaurantType />
          <RestaurantType />
          <RestaurantType />
          <RestaurantType />
          <RestaurantType />
          <RestaurantType />
          <RestaurantType />
          <RestaurantType />
          <RestaurantType />
          <RestaurantType />
          <RestaurantType />
          <RestaurantType />
          <RestaurantType />
        </div>
      </div>
    );
  }
}
