import React, { Component } from "react";
import "./Filter.css";
export class Filters extends Component {
  render() {
    return (
      <div>
        <div className="container-fluid m-4 d-flex justify-content-center">
          <div
            class="btn-toolbar"
            role="toolbar"
            aria-label="Toolbar with button groups"
          >
            <div class="btn-group" role="group" aria-label="Third group">
              <button type="button" class="btn btn-secondary">
                filter
              </button>
            </div>
            <div class="btn-group" role="group" aria-label="Third group">
              <button type="button" class="btn btn-secondary">
                rating
              </button>
            </div>
            <div class="btn-group" role="group" aria-label="Third group">
              <button type="button" class="btn btn-secondary">
                safe N hygenic
              </button>
            </div>
            <div class="btn-group" role="group" aria-label="Third group">
              <button type="button" class="btn btn-secondary">
                Pure veg
              </button>
            </div>
            <div class="btn-group" role="group" aria-label="Third group">
              <button type="button" class="btn btn-secondary">
                Delivery time
              </button>
            </div>
            <div class="btn-group" role="group" aria-label="Third group">
              <button type="button" class="btn btn-secondary">
                Great offer
              </button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Filters;
