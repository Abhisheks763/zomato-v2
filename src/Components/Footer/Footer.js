import "./Footer.css";
import React, { Component } from "react";
import GooglePlay from "../../Assets/GooglePlay.jpg";
import AppStore from "../../Assets/AppStore.svg";

export default class Footer extends Component {
  render() {
    return (
      <div className="footer">
        <div className="footer-container">
          <div className="footer-grid">
            <div className="logo">ZOMATO</div>
            <div className="country-lang">
              <button>INDIA</button>
              <button>ENGLISH</button>
            </div>
            <div className="company">
              <div className="footer-link-collection">
                <span>Company</span>
                <a className="footer-link" href="#">
                  Who We Are
                </a>
                <a className="footer-link" href="#">
                  Blog
                </a>
                <a className="footer-link" href="#">
                  Careers
                </a>
                <a className="footer-link" href="#">
                  Report Fraud
                </a>
                <a className="footer-link" href="#">
                  Contact
                </a>
                <a className="footer-link" href="#">
                  Investor Relations
                </a>
              </div>
            </div>

            <div className="foodies">
              <div className="footer-link-collection">
                <span>FOR FOODIES</span>
                <a className="footer-link" href="#">
                  Code of Conduct
                </a>
                <a className="footer-link" href="#">
                  Community
                </a>
                <a className="footer-link" href="#">
                  Blogger Help
                </a>
                <a className="footer-link" href="#">
                  Mobile Apps
                </a>
              </div>
            </div>

            <div className="for-restaurants ">
              <div className="footer-link-collection">
                <span>FOR RESTAURANTS</span>
                <a className="footer-link" href="#">
                  Add restaurant
                </a>
                <a className="footer-link" href="#">
                  Bussiness App
                </a>
                <a className="footer-link" href="#">
                  Restaurant Widgets
                </a>
                <a className="footer-link" href="#">
                  Product for Bussinesses
                </a>
              </div>
            </div>

            <div className="for-you">
              <div className="footer-link-collection">
                <span>FOR YOU</span>
                <a className="footer-link" href="#">
                  Privacy
                </a>
                <a className="footer-link" href="#">
                  Terms
                </a>
                <a className="footer-link" href="#">
                  Security
                </a>
                <a className="footer-link" href="#">
                  Sitemap
                </a>
              </div>
            </div>

            <div className="social-links">
              <div className="footer-link-collection">
                <span>SOCIAL LINKS</span>

                <div className="icons">
                  <a href="">
                    <i class="fab icon fa-twitter twt"></i>
                  </a>
                  <a href="">
                    <i class="fab icon fa-facebook-f fb"></i>
                  </a>
                  <a href="">
                    <i class="fab icon fa-instagram"></i>
                  </a>
                </div>

                <div className="badges">
                  <img className="badge-image" src={GooglePlay} />
                  <img className="badge-image" src={AppStore} />
                </div>
              </div>
            </div>
          </div>

          <hr></hr>

          <div className="toc">
            By continuing past this page, you agree to our Terms of Service,
            Cookie Policy, Privacy Policy and Content Policies. All trademarks
            are properties of their respective owners. 2008-2021 © Zomato™ Ltd.
            All rights reserved.
          </div>
        </div>
      </div>
    );
  }
}
