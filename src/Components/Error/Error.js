import React, { Component } from "react";

function Error(props) {
  return <div className="text-danger text-center mb-2">{props.error}</div>;
}

export default Error;
