import axios from "axios";

export default axios.create({
  baseURL: `https://zomato-clone-server.herokuapp.com`,
});
